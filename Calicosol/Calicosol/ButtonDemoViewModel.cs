﻿using System.Windows.Input;
using CalPeats;
using Xamarin.Forms;

namespace Calicosol
{
    public class ButtonDemoViewModel : ViewModelBase
    {
        public ButtonDemoViewModel()
        {
            this.Title = "Contact Details";
            _isEditing = false;
            this.Contact = GetContact();

            this.Address = GetAddressAsFormattedString();

            this.SaveCommand = new Command(nothing =>
            {
                IsEditing = false;
                this.Address = GetAddressAsFormattedString();
                OnPropertyChanged("Contact");
                OnPropertyChanged("PhoneAndEmail");
            });

            this.EditCommand = new Command(nothing =>
            {
                this.IsEditing = true;
            });

            this.CancelCommand = new Command(nothing =>
            {
                this.Contact = GetContact();
                IsEditing = false;
            });

            this.ClearCommand = new Command(nothing =>
            {
                this.Contact = new ContactWithPermit();
            });

        }

        private FormattedString GetAddressAsFormattedString()
        {
            var address = new FormattedString();
            var spanText = "";
            spanText = spanText.AppendIfNotNullOrEmpty(this.Contact.MailingAddress1, false);
            spanText = spanText.AppendIfNotNullOrEmpty(this.Contact.MailingAddress2);
            spanText = spanText.AppendIfNotNullOrEmpty(this.Contact.MailingCity);
            spanText = spanText.AppendIfNotNullOrEmpty(this.Contact.MailingState, false, ", ");
            spanText = spanText.AppendIfNotNullOrEmpty(this._contact.MailingZip, false, " ");

            address.Spans.Add(new Span() { Text = spanText });
            return address;
        }

        public ICommand SaveCommand { get; }

        public ICommand EditCommand { get; }

        public ICommand CancelCommand { get; }

        public ICommand ClearCommand { get; }

        public FormattedString Details { get; set; }

        public FormattedString _address;

        public FormattedString Address
        {
            get { return _address; }
            set
            {
                if (value != _address)
                {
                    _address = value;
                    OnPropertyChanged();
                }
            }
        }


        public FormattedString PhoneAndEmail
        {
            get
            {
                var phoneAndEmail = new FormattedString();
                phoneAndEmail.Spans.Add(new Span
                {
                    Text = "Primary Phone: ",
                    ForegroundColor = Color.Gray
                });
                phoneAndEmail.Spans.Add(new Span
                {
                    Text = this.Contact.PhoneMain
                });

                phoneAndEmail.Spans.Add(new Span
                {
                    Text = "\nAlternate Phone: ",
                    ForegroundColor = Color.Gray
                });
                phoneAndEmail.Spans.Add(new Span
                {
                    Text = this.Contact.PhoneOther
                });

                phoneAndEmail.Spans.Add(new Span
                {
                    Text = "\nMobile Phone: ",
                    ForegroundColor = Color.Gray
                });
                phoneAndEmail.Spans.Add(new Span
                {
                    Text = this.Contact.Cell
                });

                phoneAndEmail.Spans.Add(new Span
                {
                    Text = "\nFax: ",
                    ForegroundColor = Color.Gray
                });
                phoneAndEmail.Spans.Add(new Span
                {
                    Text = this.Contact.Fax
                });

                phoneAndEmail.Spans.Add(new Span
                {
                    Text = "\nEmail: ",
                    ForegroundColor = Color.Gray
                });
                phoneAndEmail.Spans.Add(new Span
                {
                    Text = this.Contact.Email
                });
                return phoneAndEmail;
            }
        }

        private ContactWithPermit _contact;

        public ContactWithPermit Contact
        {
            get
            {
                return _contact;
            }
            set
            {
                if (_contact != value)
                {
                    _contact = value;
                    OnPropertyChanged();
                    OnPropertyChanged("Address");
                    OnPropertyChanged("PhoneAndEmail");
                }
            }

        }

        private bool _isEditing;

        public bool IsEditing
        {
            get
            {
                return _isEditing;
            }
            set
            {
                if (value != _isEditing)
                {
                    _isEditing = value;
                    OnPropertyChanged();
                    OnPropertyChanged("IsNotEditing");
                }
            }

        }

        public bool IsNotEditing
        {
            get
            {
                return !_isEditing;
            }
        }

        private void AddDetails()
        {
            this.Details = new FormattedString();
            this.Details.Spans.Add(new Span
            {
                Text = "Integrated Pest Management",
                FontAttributes = FontAttributes.Italic
            });
            this.Details.Spans.Add(new Span
            {
                Text = "\nLicense: 64343 ",
                FontAttributes = FontAttributes.Italic
            });
            this.Details.Spans.Add(new Span
            {
                Text = "\nPrivate Applicator"
            });
        }



        private ContactWithPermit GetContact()
        {
            return new ContactWithPermit()
            {
                MailingAddress1 = "12345 Main Street",
                MailingAddress2 = "Second floor",
                MailingCity = "Monterrey",
                MailingState = "CA",
                MailingZip = "90125",
                PhoneMain = "(505) 123-1234",
                Email = "f.underwood2@gmail.com"
            };
        }
    }
}
